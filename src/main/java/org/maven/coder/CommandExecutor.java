package org.maven.coder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.maven.plugin.logging.Log;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class CommandExecutor {
    private final Log log;
    private final String command;
    private final Path input;
    private final Path output;
    private final boolean errorToOutput;

    public CommandExecutor(Log log, String command, Path input, Path output, boolean errorToOutput) {
        this.log = log;
        this.command = command;
        this.input = input;
        this.output = output;
        this.errorToOutput = errorToOutput;
    }


    public boolean run() throws IOException {
        Files.createDirectories(output.getParent());

        List<String> commandParams = generateExecutableCommand(this.command,
                Arrays.asList("${input}", "${output}"),
                Arrays.asList(input.toAbsolutePath().toString(), output.toAbsolutePath().toString()));
        log.info("Running " + commandParams);
        Process process = new ProcessBuilder(commandParams).start();

        Thread outputThread = null;
        Thread errorThread = null;
        try (StreamLogger outputLogger = new StreamLogger(process.getInputStream(), false);
             StreamLogger errorLogger = new StreamLogger(process.getErrorStream(), !errorToOutput)) {
            outputThread = new Thread(outputLogger);
            errorThread = new Thread(errorLogger);
            outputThread.start();
            errorThread.start();

            // wait for process execution
            process.waitFor();

            if (process.exitValue() != 0) {
                log.error("Process returned error code " + process.exitValue());
                return false;
            } else {
                return true;
            }
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            return false;
        } finally {
            try {
                if (outputThread != null && outputThread.isAlive()) {
                    outputThread.interrupt();
                    outputThread.join(1000);
                }
            } catch (InterruptedException e) {
                log.warn(e.getMessage(), e);
            }
            try {
                if (errorThread != null && errorThread.isAlive()) {
                    errorThread.interrupt();
                    errorThread.join(1000);
                }
            } catch (InterruptedException e) {
                log.warn(e.getMessage(), e);
            }
        }

    }

    public List<String> generateExecutableCommand(String initialCommand, List<String> searchItems, List<String> replaceItems) {
        List<String> commandParams = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer(initialCommand);
        while(tokenizer.hasMoreElements()) {
            String token = tokenizer.nextToken();
            int index;
            if((index = searchItems.indexOf(token)) != -1) {
                token = replaceItems.get(index);
            }
            commandParams.add(token);
        }

        return commandParams;
    }

    public class StreamLogger implements Runnable, Closeable {
        private final BufferedReader br;
        private final boolean error;

        public StreamLogger(InputStream is, boolean error) {
            this.br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            this.error = error;
        }

        @Override
        public void run() {
            String s;
            try {
                while ((s = br.readLine()) != null) {
                    if (error) {
                        log.error(s);
                    } else {
                        log.info(s);
                    }
                }
            } catch (IOException e) {
                // Stream closed is an expected error, don't log it
                if (!e.getMessage().contains("Stream closed")) {
                    log.error(e.getMessage(), e);
                }
            }
        }

        @Override
        public void close() throws IOException {
            br.close();
        }
    }

    public static Path replaceExtension(Path output, String outputExtension) {
        String filename = output.getFileName().toString();
        int idx = filename.lastIndexOf('.');
        String filenameWithoutExtension;
        if (idx > 0) {
            filenameWithoutExtension = filename.substring(0, idx);
        } else {
            filenameWithoutExtension = filename;
        }
        String filenameWithExtension = filenameWithoutExtension + "." + outputExtension;
        return output.getParent().resolve(filenameWithExtension);
    }

}
